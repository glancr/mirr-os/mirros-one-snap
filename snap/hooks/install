#!/bin/sh -e

# shellcheck source=src/mysql/scripts/mysql-utilities
. "$SNAP/scripts/mysql-utilities"

# Initialize database
init_database

snapctl start mirros-one.mysql

wait_for_mysql 30

set_root_password

if mysql_test_connection; then
  echo "mysql connection test successful"
else
  echo "mysql connection test failed, bailing"
  exit 1
fi

# nginx setup. Limit substitution to prevent overwriting nginx variables.
mkdir -p "$SNAP_COMMON/nginx/log"
envsubst '${SNAP} ${SNAP_NAME} ${SNAP_COMMON}' >"$SNAP_COMMON/nginx.conf" <"$SNAP/nginx.conf"

# Create directories for redis in writable area.
mkdir -p "$SNAP_COMMON/redis/db"

# Rails app setup
cd "$SNAP/api"

# Ensure that all commands are run in prod environment.
export RAILS_ENV=production

# Generates an individual secret for each snap installation. Environment variables set here might not be present at all times across revisions, so we store it in a file. TODO: Check if we can leverage snap configuration.
"$SNAP/api/bin/rails" secret >"$SNAP_DATA/secret"

SECRET_KEY_BASE=$(cat "${SNAP_DATA}/secret")
export SECRET_KEY_BASE

# Set up the API database.
"$SNAP/api/bin/rails" db:create db:migrate db:seed

# If the network-manager plug is already connected, we can set up the connections. This is the case when seeding via our gadget.
if snapctl is-connected network-manager; then
  # AP + persistent and predictable LAN connection name
  "$SNAP/api/bin/rails" mirros:setup:network_connections
fi
