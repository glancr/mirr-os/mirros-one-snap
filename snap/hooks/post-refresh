#!/bin/bash -e

# 1.8.13: Redis server for ActionCable requires writable directory.
mkdir -p "$SNAP_COMMON/redis/db" || true

# 1.10.0: Migrate MySQL directory
if [ -d "$SNAP_COMMON/mysql" ] && [ ! -d "$SNAP_DATA/mysql" ]; then
  # TODO: Delete leftover database directories in later snap revisions.
  # We keep them around for now to ensure pre-1.10.0 installations have a working database in case the post-refresh hook errors out *after* the database directories have been deleted.
  # rm -rf "$SNAP_COMMON/mysql/run" || true # now uses /tmp for pid/socket files
  # rm -rf "$SNAP_COMMON/mysql/conf" || true # my.cnf now bundled in $SNAP
  echo "Copying MySQL directory to SNAP_DATA"
  cp -r "$SNAP_COMMON/mysql" "$SNAP_DATA"

  echo "Updating log directory path"
  cd "$SNAP_DATA/mysql"
  mv log logs
  echo "Setting permissions for log directory"
  chmod -R 750 logs
fi

# shellcheck source=src/mysql/scripts/mysql-utilities
. "$SNAP/scripts/mysql-utilities"

snapctl start mirros-one.mysql
wait_for_mysql 360

# Clear the redis caches to prevent stale or outdated entries from messing with new features. Do not abort the hook if the connection fails.
snapctl start mirros-one.redis
sleep 10
redis-console 'FLUSHALL' || true

cd "$SNAP"/api # required for bundler to find the gemfile

SECRET_KEY_BASE=$(cat "$SNAP_DATA"/secret)
export SECRET_KEY_BASE
export RAILS_ENV=production

"$SNAP/api/bin/rails" db:migrate

# Seed new or changed seed values for running systems.
"$SNAP"/api/bin/rails db:seed_diff
"$SNAP"/api/bin/rails db:update_default_gems

# Overwrite existing nginx configuration: $SNAP points to a specific revision.
envsubst '${SNAP} ${SNAP_NAME} ${SNAP_COMMON}' >"$SNAP_COMMON/nginx.conf" <"$SNAP/nginx.conf"
