name: mirros-one
adopt-info: snap-meta
summary: An operating system tailored for smart mirrors.
description: |
  mirr.OS is the operating system for the glancr smart mirror,
  but can be used for other kiosk/display-only devices.
  Currently meant to be deployed on a Raspberry Pi (min model 3B).
  Learn more at https://glancr.de
confinement: strict
base: core20
environment:
  GEM_HOME: $SNAP/lib/ruby/gems/2.6.0 # read-only, but we do not install gems at runtime
  GEM_PATH: $GEM_HOME
  RUBYLIB: $SNAP/lib/ruby/2.6.0:$SNAP/lib/ruby/2.6.0/$SNAP_ARCH:$RUBYLIB # do not use triplet, the ruby lib has no -gnu suffix
  RAILS_DATABASE_SOCKET: "/tmp/sockets/mysqld.sock"
  REDIS_URL: "unix:///tmp/sockets/redis-server.sock"

# Hooks need explicit environment setup through snapcraft-runner.
hooks:
  install:
    command-chain:
      - snap/command-chain/snapcraft-runner
      - bin/create-tmp-directories
    # Make the list of common plugs reusable
    plugs: &allplugs
      - dbus-cogctl
      - network
      - network-bind
      - network-control
      - network-manager
      # - network-observe
      - shutdown
        # backend sets time-related system settings
      - timezone-control
      - timeserver-control
      - time-control

  pre-refresh:
    command-chain:
      - snap/command-chain/snapcraft-runner
      - bin/create-tmp-directories
    plugs: *allplugs

  post-refresh:
    command-chain:
      - snap/command-chain/snapcraft-runner
      - bin/create-tmp-directories
    plugs: *allplugs

  connect-plug-network-manager:
    command-chain:
      - snap/command-chain/snapcraft-runner
      - bin/create-tmp-directories
    plugs: *allplugs

plugs:
  dbus-cogctl:
    interface: dbus
    bus: system
    name: com.igalia.Cog

apps:
  # SERVICES
  mysql:
    command-chain: [bin/create-tmp-directories]
    command: bin/start-mysql-server
    daemon: simple
    restart-condition: always
    plugs:
      - network
      - network-bind
      - process-control

  redis:
    command-chain: [bin/create-tmp-directories]
    command: usr/bin/redis-server $SNAP/redis.conf
    daemon: simple
    restart-condition: always
    plugs:
      - network
      - network-bind
      #- network-observe

  sidekiq:
    after: [mysql, redis]
    command: bin/start-sidekiq
    daemon: simple
    restart-condition: always
    plugs: *allplugs

  backend:
    after: [mysql, nginx, redis, sidekiq]
    command: bin/start-backend
    daemon: simple
    restart-condition: always
    plugs: *allplugs

  nginx:
    command-chain: [bin/create-tmp-directories]
    command: usr/sbin/nginx -c $SNAP_COMMON/nginx.conf
    daemon: forking
    stop-command: usr/sbin/nginx -s stop
    restart-condition: always
    plugs:
      - network
      - network-bind

  dns:
    command: bin/start-dns
    daemon: simple
    install-mode: disable
    plugs: *allplugs

  reload-browser:
    after: [nginx, redis, backend]
    command: bin/reload-browser
    daemon: oneshot
    install-mode: disable
    plugs: [dbus-cogctl]

  # COMMANDS
  db-console:
    command: bin/run-mysql-client mirrOS_api_production
  rails-console:
    command: bin/rails-console
    plugs: *allplugs
  redis-console:
    command: bin/redis-console
    plugs:
      - network
      - network-bind
      #- network-observe
  create-backup:
    command: bin/create-backup
  restore-backup:
    command: bin/restore-backup

layout:
  $SNAP/api/storage:
    bind: $SNAP_DATA/rails/storage
  $SNAP/api/tmp:
    bind: $SNAP_DATA/rails/tmp
  $SNAP/api/log:
    bind: $SNAP_DATA/rails/log
  /etc/ImageMagick-6:
    bind: $SNAP/etc/ImageMagick-6
  /usr/lib/$SNAPCRAFT_ARCH_TRIPLET/ImageMagick-6.8.9:
    bind: $SNAP/usr/lib/$SNAPCRAFT_ARCH_TRIPLET/ImageMagick-6.8.9
    # FIXME: can this be generalized for 6.x.x releases?

parts:
  snap-meta:
    plugin: nil
    source: .
    source-type: git
    override-build: |
      SNAP_REVISION=$(git describe --always)
      snapcraftctl set-version "$SNAP_REVISION"
      # scriptlets are run with /bin/sh -e, i.e. no bash pattern matching
      if echo "$SNAP_REVISION" | grep -q -E '^[0-9]+.[0-9]+.[0-9]+$'; then
        snapcraftctl set-grade stable
      else
        snapcraftctl set-grade devel
      fi

  # Provides all git patches in $SNAPCRAFT_STAGE, but excludes them from priming
  patches:
    plugin: dump
    source: src/patches
    prime:
      - -*

  mysql:
    plugin: dump
    source: src/mysql
    stage-packages: [mysql-server, libaio1]

  nginx:
    plugin: dump
    source: src/nginx
    stage-packages: [nginx, gettext-base]

  redis:
    plugin: dump
    source: src/redis
    stage-packages:
      - redis-server

  dnsmasq:
    #after: [patches]
    plugin: make
    source: https://git.launchpad.net/~snappy-hwe-team/snappy-hwe-snaps/+git/wifi-ap
    source-type: git
    source-branch: dnsmasq/2.75
    #source: http://www.thekelleys.org.uk/dnsmasq/dnsmasq-2.80.tar.gz
    build-packages:
      - build-essential
    make-parameters:
      - PREFIX=/
    #override-pull: |
    #  snapcraftctl pull
    #  git apply "${SNAPCRAFT_STAGE}/dnsmasq_2.80-remove_setgroups.patch"
    organize:
      sbin/dnsmasq: bin/dnsmasq
    filesets:
      binaries:
        - bin/dnsmasq
    prime:
      - $binaries

  dnsmasq-config:
    source: src/dnsmasq
    plugin: dump

  mirros-display:
    source: https://gitlab.com/glancr/mirr-os/mirros_display.git
    source-type: git
    source-depth: 1
    source-tag: v1.11.0
    plugin: npm
    npm-node-version: "18.12.1"
    override-build: |
      snapcraftctl build
      mv $SNAPCRAFT_PART_INSTALL/lib/node_modules/mirros_display/ $SNAPCRAFT_PART_INSTALL/
      cd $SNAPCRAFT_PART_INSTALL/mirros_display
      corepack enable
      yarn install
      yarn run build --modern
      cp -r dist/ $SNAPCRAFT_PART_INSTALL
    organize:
      dist: display
    stage:
      - display

  mirros-settings:
    source: https://gitlab.com/glancr/mirr-os/mirros_settings.git
    source-type: git
    source-depth: 1
    source-tag: v1.10.0
    plugin: npm
    npm-node-version: "18.12.1"
    override-build: |
      snapcraftctl build
      mv $SNAPCRAFT_PART_INSTALL/lib/node_modules/mirros_settings/ $SNAPCRAFT_PART_INSTALL/
      cd $SNAPCRAFT_PART_INSTALL/mirros_settings
      corepack enable
      yarn install
      yarn run build --modern
      cp -r dist/ $SNAPCRAFT_PART_INSTALL
    organize:
      dist: settings
    stage:
      - settings

  mirros-api-deps:
    plugin: nil
    source: https://github.com/rbenv/ruby-build.git
    source-tag: v20211227
    source-depth: 1
    build-packages:
      - libmysqlclient-dev
        # ruby-build dependencies
      - libcurl4
      - autoconf
      - bison
      - build-essential
      - libssl-dev
      - libyaml-dev
      - libreadline6-dev
      - zlib1g-dev
      - libncurses5-dev
      - libffi-dev
      - libgdbm6
      - libgdbm-dev
      - libdb-dev
    stage-packages:
      - libatm1
      - libcurl4
      - libmysqlclient21
      - wireless-tools # iwlist for scanning while AP is active
      - dbus # provides dbus-send for shutdown command
      # Image resizing
      - graphicsmagick-imagemagick-compat
      - imagemagick-6-common
    override-build: |
      snapcraftctl build
      PREFIX=$SNAPCRAFT_STAGE ./install.sh
      $SNAPCRAFT_STAGE/bin/ruby-build 2.6.9 $SNAPCRAFT_PRIME/

    override-stage: |

      snapcraftctl stage

      # Ruby expects an architecture-specific lib directory without the gnu suffix, so we symlink to it
      case "$SNAPCRAFT_ARCH_TRIPLET" in
      "x86_64-linux-gnu")
        target="x86_64-linux"
        arch="amd64"
        ;;
      "arm-linux-gnueabihf")
        target="armv7l-linux-eabihf"
        arch="armhf"
        ;;
      "aarch64-linux-gnu")
        target="aarch64-linux"
        arch="arm64"
        ;;
      "i386-linux-gnu")
        target="i686-linux"
        arch="i386"
        ;;
      *)
        echo "Unsupported architecture $SNAPCRAFT_ARCH_TRIPLET"
        exit 1
        ;;
      esac
      root="$SNAPCRAFT_PRIME/lib/ruby/2.6.0"
      ln -s "$target" "$root/$arch"
    stage:
      - -usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/libgdbm.so.6.0.0 # conflicts with mysql part

  mirros-api:
    after: [mirros-api-deps]
    source: https://gitlab.com/glancr/mirr-os/mirros_api.git
    source-type: git
    source-depth: 1
    source-tag: 1.14.4
    plugin: dump
    override-build: |
      git describe --always > build_version

      # Calling bundler through snap command ensures proper environment setup
      $SNAPCRAFT_PRIME/bin/bundle install \
        --deployment \
        -j $(nproc) \
        --without=development test

      snapcraftctl build

      # Replace build-snap ruby path with our snap path to ensure ruby scripts call the correct binary
      find bin/ -type f -exec grep -Iq . {} \; -and -exec sed -i -e 's|^#!/bin/ruby$|#!/snap/mirros-one/current/bin/ruby|' {} \;

      # Remove directory stubs which are mapped to writable $SNAP_DATA in layout
      rm -rf $SNAPCRAFT_PART_INSTALL/tmp $SNAPCRAFT_PART_INSTALL/log
    stage:
      - -test
      - -docs
    organize:
      "*": api/
      # python glob does not include hidden files
      ".ruby-version": api/
      ".bundle/": api/.bundle/ # required for bundler to find the gems in vendor/bundle

  # Scripts and wrappers
  scripts:
    source: src/backend
    plugin: dump
  cli:
    source: src/cli
    plugin: dump
